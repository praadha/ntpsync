@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            
            @isset($output)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ $output }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
            @endisset
            
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="margin: 0 -20px; padding: 0 20px;">
                            <a class="nav-item nav-link active px-5" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Admin</a>
                            <a class="nav-item nav-link px-5" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Network</a>
                            <a class="nav-item nav-link px-5" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">NTP</a>
                            <a class="nav-item nav-link px-5" id="nav-contact-tab" data-toggle="tab" href="#nav-hostname" role="tab" aria-controls="nav-hostname" aria-selected="false">Hostname</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="row">
                                <div class="col-md-6 col-12 py-3">
                                    
                                    <ul class="nav flex-column">
                                        @if (Auth::check() && Auth::user()->role == 'admin')
                                        <li class="nav-item">
                                            <a href="{{route('register')}}" class="nav-link" >Register new user</a>
                                            {{-- <a class="nav-link active" href="#">Active</a> --}}
                                        </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('rp')}}">Change Password</a>
                                        </li>
                                    </ul>
                                    {{-- @else
                                        You are a User! --}}

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="row">
                                <div class="col-md-6 col-12 py-3">
                                    <form action="{{route('shell.network')}}" method="post" name="networkForm">
                                        @csrf
                                        {{-- <div class="form-group row">
                                            <label for="ntpserver" class="col-form-label col-4 text-right">NTP Server</label>
                                            <div class="col-8">
                                                <input type="text" name="ntpserver" id="ntpserver" required 
                                                @if (Auth::check() && Auth::user()->role == 'admin')
                                                    class="form-control"
                                                @else
                                                    class="form-control-plaintext" readonly
                                                @endif
                                                >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="timezone" class="col-form-label col-4 text-right">Timezone : </label>
                                            <div class="col-8">
                                                <input type="text" name="timezone" id="timezone" class="form-control" required>
                                            </div>
                                        </div> --}}
                                        <div class="form-group row">
                                            <label for="ipaddress" class="col-form-label col-4 text-right">IP Address : </label>
                                            <div class="col-8 d-flex align-content-center">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="customRadioInline1" name="ipadd" class="custom-control-input" required checked value="dhcp">
                                                    <label class="custom-control-label" for="customRadioInline1">DHCP</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="customRadioInline2" name="ipadd" class="custom-control-input" required value="static">
                                                    <label class="custom-control-label" for="customRadioInline2">Static</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <fieldset id="is-disabled" disabled="disabled">
                                            <div class="form-group row">
                                                <label for="staticip" class="col-form-label col-4 text-right">Static IP : </label>
                                                <div class="col-8">
                                                    <input type="text" name="staticip" id="staticip" class="form-control ip-address" pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" placeholder="___.___.___.___" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="netmask" class="col-form-label col-4 text-right">Netmask : </label>
                                                <div class="col-8">
                                                    <input type="text" name="netmask" id="netmask" class="form-control ip-address" pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" placeholder="___.___.___.___" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="gateway" class="col-form-label col-4 text-right">Gateway : </label>
                                                <div class="col-8">
                                                    <input type="text" name="gateway" id="gateway" class="form-control ip-address" pattern="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" placeholder="___.___.___.___" required>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-4"></div>
                                            <div class="col-8">
                                                <p id="passwordHelpBlock" class="form-text text-muted">
                                                    Required to reboot devices!
                                                </p>
                                            </div>

                                        </div>
                                        @if (Auth::check() && Auth::user()->role == 'admin')
                                        <div class="form-group row justify-content-center">
                                            <div class="col-12 col-sm-4">
                                                <input type="submit" value="Save" class="btn btn-primary btn-block">
                                            </div>
                                        </div>
                                            
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="row">
                                <div class="col-md-6 col-12 py-3">
                                    <form action="{{route('shell.ntp')}}" method="post">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="ntpserver" class="col-form-label col-4 text-right">NTP Server</label>
                                            <div class="col-8">
                                                <input type="text" name="ntpserver" id="ntpserver" required 
                                                @if (Auth::check() && Auth::user()->role == 'admin')
                                                    class="form-control"
                                                @else
                                                    class="form-control-plaintext" readonly
                                                @endif
                                                >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="timezone" class="col-form-label col-4 text-right">Timezone : </label>
                                            <div class="col-8">
                                                <input type="number" name="timezone" id="timezone" required maxlength="4" pattern="\d{1,2}" min="1" max="99"
                                                @if (Auth::check() && Auth::user()->role == 'admin')
                                                    class="form-control"
                                                @else
                                                    class="form-control-plaintext" readonly
                                                @endif
                                                >
                                            </div>
                                        </div>
                                        @if (Auth::check() && Auth::user()->role == 'admin')
                                        <div class="form-group row justify-content-center">
                                            <div class="col-12 col-sm-4">
                                                <input type="submit" value="Save" class="btn btn-primary btn-block">
                                            </div>
                                        </div>
                                            
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-hostname" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="row">
                                <div class="col-md-6 col-12 py-3">
                                    <form action="{{route('shell.hostname')}}" method="post">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="hostname" class="col-form-label col-4 text-right">Hostname</label>
                                            <div class="col-8">
                                                <input type="text" name="hostname" id="hostname" required 
                                                @if (Auth::check() && Auth::user()->role == 'admin')
                                                    class="form-control"
                                                @else
                                                    class="form-control-plaintext" readonly
                                                @endif
                                                >
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4"></div>
                                            <div class="col-8">
                                                <p id="passwordHelpBlock" class="form-text text-muted">
                                                    Required to reboot devices!
                                                </p>
                                            </div>
                                        </div>
                                        @if (Auth::check() && Auth::user()->role == 'admin')
                                        <div class="form-group row justify-content-center">
                                            <div class="col-12 col-sm-4">
                                                <input type="submit" value="Save" class="btn btn-primary btn-block">
                                            </div>
                                        </div>
                                            
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>
    $('document').ready(function($) {
            $('input[name="ipadd"]').on('change', function(e) {
                if (e.target.value == 'static') {
                    document.getElementById('is-disabled').disabled = false;
                }
                else {
                    document.getElementById('is-disabled').disabled = true;
                }
            });

            $('.ip-address').mask('099.099.099.099');

        });

    </script>
@endsection