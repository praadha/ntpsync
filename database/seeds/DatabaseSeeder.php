<?php

use Illuminate\Database\Seeder;
use App\User;
use App\ntp_server;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('PasswordAdmin1!'),
            'role' => 'admin',
        ]);

        $admin->save();

        $ntp = new ntp_server([
            'ipaddress' => 'dhcp',
            'ntpserver' => '',
            'timezone' => 0,
            'hostname' => '',
        ]);

        $ntp->save();
    }
}
