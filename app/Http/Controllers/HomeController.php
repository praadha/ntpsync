<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\ntp_server;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ntp = ntp_server::findorfail(1);
        $output = 'lala';
        return view('home', compact('ntp'));
    }

    public function changePassword() {
        $email = Auth::user()->email;
        return view('auth.reset', compact('email'));
    }

    public function updatePassword(Request $request) {
        
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string|min:10|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);
        $response = 'lala';

        $user = User::where('email', $request->email)->first();
        $user->password =  Hash::make($request->password);
        $user->save();

        // return $user;
        return redirect()->route('home');
        
        // return redirect()->back()
        //             ->withInput($request->only('email'))
        //             ->withErrors(['email' => trans($response)]);
    }

}
