<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\ntp_server;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


class ShellController extends Controller
{
    public function shellNetwork (Request $request) 
    { 
        $output = NULL;
        // return $request;
        if ($request->ipadd == 'dhcp') {
            $ntp = ntp_server::findorfail(1);
            $ntp->ipaddress = 'dhcp';
            $ntp->staticip = null;
            $ntp->netmask = null;
            $ntp->gateway = null;
            
            $process = new Process('sh /var/www/html/ntpsync/storage/script/net_dhcp.sh');
            $process->run();
            
            $ntp->save();
            // return $ntp;
        }
        elseif ($request->ipadd == 'static') {

            $arg1 = $request->staticip;
            $arg2 = $request->netmask;
            $arg3 = $request->gateway;

            
            $ntp = ntp_server::findorfail(1);
            $ntp->ipaddress = 'static';
            $ntp->staticip = $request->staticip;
            $ntp->netmask = $request->netmask;
            $ntp->gateway = $request->gateway;

            
            // run script static
            // $old_path = getcwd();
            // chdir('../storage/');
            // $output = shell_exec('sudo ./net_dhcp.sh '.$arg1.' '.$arg2.' '.$arg3);
            // chdir($old_path);

            $ntp->save();
            // return array(
            //     'arg1' => $arg1,
            //     'arg2' => $arg2,
            //     'arg3' => $arg3,
            // );
        }
        
        return redirect()->route('home')->with('status', $output);
        // return view('home', compact('ntp', 'output'));
    }

    public function shellNtp (Request $request) {

        $ntp = ntp_server::findorfail(1);
        $ntp->ntpserver = $request->ntpserver; 
        $ntp->timezone = $request->timezone;

        
        // run script ntp
        $old_path = getcwd();
        chdir('../storage/');
        $output = shell_exec('sudo python getntp.py '.$request->ntpserver.' '.$request->timezone);
        chdir($old_path);

        $ntp->save();


        return redirect()->route('home', compact('ntp'));
        // return array(
        //     'ntp-server' => 'ntp.its.ac.id',
        //     'timezone' => '7'
        // );
    }

    public function shellHostname (Request $request) {

        $nama = $request->hostname;
        // run script hostname
        $old_path = getcwd();
        chdir('../storage');
        $output = shell_exec('sudo ./change_hostname.sh '.$nama);
        chdir($old_path);

        $ntp = ntp_server::findorfail(1);
        $ntp->hostname = $request->hostname; 

        $ntp->save();


        return redirect()->route('home', compact('ntp'));
    }
}
