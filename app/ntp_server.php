<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ntp_server extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ipaddress', 'staticip', 'netmask', 'gateway', 'ntpserver', 'timezone', 'hostname',
    ];
}
